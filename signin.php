<?php
echo "<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Document</title>
    <link rel='stylesheet' type='text/css' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'/>
    <link rel='stylesheet' type='text/css' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css' />
    <link rel='stylesheet' type='text/css' href='signin.css'>
    
</head>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js'></script>
<body>";


echo "<form id='body' method='post' action='" . htmlspecialchars($_SERVER["PHP_SELF"]) . "'>";
$msg = array();
$msg["Name"] = "<br>";
$msg["Gender"] = "<br>";
$msg["Faculty"] = "<br>";
$msg["DateBirth"] = "<br>";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["Name"])) {
        $msg["Name"] = "<span class='error'>Hãy nhập tên.</span>
            <br>";
    }
    if (!isset($_POST["Gender"])) {
        $msg["Gender"] = "<span class='error'>Hãy chọn giới tính.</span>
            <br>";
    }
    if (empty($_POST["Faculty"])) {
        $msg["Faculty"] = "<span class='error'>Hãy chọn phân khoa.</span>
            <br>";
    }
    if (empty($_POST["DateBirth"])) {
        $msg["DateBirth"] = "<span class='error'>Hãy nhập ngày sinh.</span>
            <br>";
    }
}
echo $msg["Name"];
echo $msg["Gender"];
echo $msg["Faculty"];
echo $msg["DateBirth"];
echo "
        
        <table>
            <tr>
                <td class='background_blue'><label class='required'> Họ và tên </label> </td>
                <td> <input type='text' class='frames' style=' width: 120%;' name='Name' value='";
echo isset($_POST['Name']) ? $_POST['Name'] : '';
echo "'></td>
            </tr>

            <tr>
                <td class='background_blue'> <label class='required'> Giới tính </label> </td>
                <td>";
$gender = array(0 => 'Nam', 1 => 'Nữ');
for ($i = 0; $i < count($gender); $i++) {
    echo "<input type='radio'  name='Gender' value='" . $i . "'";
    echo isset($_POST['Gender']) && $_POST['Gender'] == $i ? " checked" : "";
    echo " /> " . $gender[$i];
}
echo "</td>
            </tr>
            <tr>
                <td class='background_blue'>
                <label class='required'> Phân Khoa </label>
                </td>
                <td> <select class='blue-box' name='Faculty'>";
$faculty = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
foreach ($faculty as $key => $value) {
    echo " <option ";
    echo isset($_POST['Faculty']) && $_POST['Faculty'] == $key ? "selected " : "";
    echo "value ='" . $key . "'>" . $value . "</option> ";
}
echo "</td>
                </td>
            </tr>
            <tr> 
                <td class='background_blue'> <label class='required'> Ngày sinh </label></td>
                <td> <input type='text' onkeydown='return false' class='date form-control blue-box' name='DateBirth' placeholder='dd/mm/yyyy' value='";
echo isset($_POST['DateBirth']) ? $_POST['DateBirth'] : "";
echo "'>
                <script type='text/javascript'>
                    $('.date').datepicker({
                        format: 'dd/mm/yyyy',
                    });
                </script>
                </td>
            </tr>

            <tr>
                <td class='background_blue'> <label> Địa chỉ </label> </td>
                <td> <input type='text' class='frames' style=' width: 120%; ' name='Address' value='";
echo isset($_POST['Address']) ? $_POST['Address'] : "";
echo "'></td>
            </tr>
        </table>
        <input type='submit' name='submit' value='Đăng kí' class='submit'>
    </form>

</body>

</html>";
